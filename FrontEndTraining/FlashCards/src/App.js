import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './App.css';
import FlashCards from './pages/FlashCards/flashCards';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<FlashCards/>} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
