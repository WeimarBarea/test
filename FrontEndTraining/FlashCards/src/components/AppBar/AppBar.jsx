import AppBar from '@mui/material/AppBar';
export default function MenuBar({children,position}){
    return(
        <AppBar position={position}>
             {children}
        </AppBar>
    )
}