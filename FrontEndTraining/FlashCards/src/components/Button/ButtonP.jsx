import Button from '@mui/material/Button';

export default function ButtonP({children, size, action, sx, variant, disabled}){
    return(
        <Button onClick={() => action()} size={size} sx={sx} variant={variant} disabled={disabled}>
            {children}
        </Button>
    )
}