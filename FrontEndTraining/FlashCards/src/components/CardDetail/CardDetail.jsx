import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Text from '../Text/Text';
import ButtonP from '../Button/ButtonP';

export default function CardDetail({title, content, buttonAction, buttonMessage}){
    return(
        <Card sx={{widt: 275, height: 200, margin: 2 }}>
            <CardContent sx={{height: 120, overflow: "auto"}}>
                <Text variant="h5">
                    {title}
                </Text>
                <Text variant="body2">
                    {content}
                </Text>
            </CardContent>
            <CardActions>
                <ButtonP action={buttonAction} size="small">{buttonMessage}</ButtonP>
            </CardActions>
        </Card>
    )
}