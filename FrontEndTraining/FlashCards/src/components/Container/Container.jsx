import Box from '@mui/material/Box';

export default function Container({children, sx}){
    return(
        <Box sx={sx}>
            {children}
        </Box>
    )
}