import Dialog from '@mui/material/Dialog';


export default function Form({open, children, handleClose}){
    return(
      <Dialog open={open} onClose={handleClose}>
          {children}
      </Dialog>
    )
}