import DialogActions from '@mui/material/DialogActions';

export default function FormActions({children}){
    return(
        <DialogActions>
            {children}
        </DialogActions>
    )
}