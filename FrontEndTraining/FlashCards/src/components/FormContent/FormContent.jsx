import DialogContent from '@mui/material/DialogContent';

export default function FormContent({children}){
    return(
        <DialogContent>
            {children}
      </DialogContent>
    )
}