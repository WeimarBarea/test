import DialogTitle from '@mui/material/DialogTitle';

export default function FormTitle({children}){
    return(
        <DialogTitle>{children}</DialogTitle>
    )
}