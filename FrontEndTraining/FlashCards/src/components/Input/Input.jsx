import TextField from '@mui/material/TextField';
import { useState } from 'react';

export default function BasicInput({helpMessage, validator, errorInitital, margin, variant, label, setWord, word, name ,setDisabledButton}){
    const[error, setError] = useState(errorInitital);

    function handleInput(e){
        setWord({
            ...word,
            [name]: e.target.value
        });
        if(validator(e.target.value)){
            setError(false);
            setDisabledButton(false);
        }else{
            setError(true);
            setDisabledButton(true);
        }
    }
    return(
        <TextField 
        sx={{marginRight: 2}}
        error={error} 
        label={error ? 'error' : label} 
        id="outlined-error-helper-text"
        helperText={error ? helpMessage: null} 
        value={word[name]} 
        onChange={(e) => handleInput(e)} 
        margin={margin}
        variant={variant}
        />
    )
}