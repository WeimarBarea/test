import Typography from '@mui/material/Typography';

export default function Text({children, variant, fontFamily,component, sx}){
    return(
        <Typography fontFamily={fontFamily} variant={variant} component={component} sx={sx}>
            {children}
        </Typography>
    )
}