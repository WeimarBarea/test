import { persistance } from "../helpers/persistance";
import { ActionsTypes } from "./constans";

function getCards(dispatch){
    dispatch({ type: ActionsTypes.LOADING_SWITCH, payload: true});
    persistance.getCardsAsync()
    .then(response => dispatch({ type: ActionsTypes.CARD_LOAD, payload: response }))
    .finally(() => dispatch({ type: ActionsTypes.LOADING_SWITCH, payload: false}));
}

function createCard(dispatch, card){
    dispatch({ type: ActionsTypes.LOADING_SWITCH, payload: true});
    persistance.createCardAsync(card)
    .then(response => dispatch({ type: ActionsTypes.CARD_CREATE, payload: response}))
    .finally(() => dispatch({ type: ActionsTypes.LOADING_SWITCH, payload: false}));
}

function deleteCard(dispatch, id){
    dispatch({ type: ActionsTypes.LOADING_SWITCH, payload: true});
    persistance.deleteCardAsync(id)
    .then(response => dispatch({ type: ActionsTypes.CARD_DELETE, payload: response }))
    .finally(() => dispatch({ type: ActionsTypes.LOADING_SWITCH, payload: false}));
}

export default function ActionFactory(dispatch){
    return{
        getCards: () => getCards(dispatch),
        createCard: (title) => createCard(dispatch, title),
        deleteCard: (id) => deleteCard(dispatch, id)
    }
}