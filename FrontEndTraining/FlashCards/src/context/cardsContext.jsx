import { createContext, useReducer, useMemo } from "react";
import ActionFactory from "./actions";
import cardsReducer from "./cardsReducer";

export const CardsContext = createContext();
export const DispatchContext = createContext();

const initialState = { cards: [], loading: false };

export default function CardProvider({children}){
    const[cards, dispatch] = useReducer(cardsReducer, initialState);
    const actions = useMemo(() => ActionFactory(dispatch), [dispatch]);

    return(
        <CardsContext.Provider value={cards}>
            <DispatchContext.Provider value={actions}>
                {children}
            </DispatchContext.Provider>
        </CardsContext.Provider>
    )
}