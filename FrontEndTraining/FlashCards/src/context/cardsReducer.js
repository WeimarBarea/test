import { ActionsTypes } from "./constans";

export default function cardsReducer(state, action){
    const {type, payload} = action;
    switch(type){
        case ActionsTypes.CARD_LOAD: {
            return {...state, cards: payload};
        }
        case ActionsTypes.CARD_CREATE: {
            return {
                ...state,
                cards: [
                    ...state.cards,
                    payload
                ]}
        }
        case ActionsTypes.CARD_DELETE: {
            return {...state, cards: state.cards.filter(card => card.id !== payload)};
        }
        case ActionsTypes.LOADING_SWITCH: {
            return {...state, loading: payload};
        }
        default:{
            return {...state};
        }
    }
}