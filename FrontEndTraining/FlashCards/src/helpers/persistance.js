import { storageUtils } from "../utils/storageUtils";
import initialCards from "../data/cards.json";
import { uuidv4 } from "../utils/uuid";

const CARDS_KEY = 'cards';

function getCardsAsync(){
    let cards = storageUtils.getItems(CARDS_KEY);
    if(cards.length === 0){
        cards = initialCards;
        storageUtils.saveITems(CARDS_KEY, cards);
    }

    return new Promise((resolve, reject) => {
        if(cards.length === 0){
            reject(new Error('empty values.'));
        }
        setTimeout(() => {
            resolve(cards);
        }, 1500);
    })
}

function createCardAsync({title, content}){
    let uuid = uuidv4();
    let card = {
        title: title,
        id: uuid,
        created: Date.now(),
        topicId: "572269fd-50b6-43af-87e9-d21421d7234f",
        content: content
    }
    storageUtils.saveItem(CARDS_KEY, card);

    return new Promise((resolve, reject) => {
        if(!card.id){
            reject(new Error('Error'));
        }
        setTimeout(() => {
            resolve(card);
        }, 1500)
    })
}

function deleteCardAsync(id){
    storageUtils.deleteItem(CARDS_KEY, (card => card.id !== id));

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(id)
        }, 1500)
    })
}

export const persistance = {
    getCardsAsync: () => getCardsAsync(),
    createCardAsync: (title) => createCardAsync(title),
    deleteCardAsync: (id) => deleteCardAsync(id)
}