import Cards from "./layouts/cards"
import CardProvider from "../../context/cardsContext"
import ActionBar from "./layouts/actionBar"

export default function FlashCards(){

    return(
        <CardProvider>
            <ActionBar/>
            <Cards/>
        </CardProvider>
    )
}