import CreateCardForm from "./createCardForm";
import MenuBar from '../../../components/AppBar/AppBar';
import Text from '../../../components/Text/Text';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
export default function ActionBar(){
    return(
        <Box sx={{ flexGrow: 1 }}>
            <MenuBar position="static">
            <Toolbar>
                <Text variant="h6" component="div" sx={{ flexGrow: 1 }}>
                Flashcards
                </Text>
                <CreateCardForm>
                Create Card
                </CreateCardForm>
            </Toolbar>
            </MenuBar>
        </Box>  
    )
}