import { useContext, useEffect } from 'react';
import Grid from '@mui/material/Grid';
import CardDetail from '../../../components/CardDetail/CardDetail';
import Container from '../../../components/Container/Container';
import { CardsContext, DispatchContext } from '../../../context/cardsContext';
import LinearProgress from '../../../components/LinnerProgress/LinearProgress';
let isLoaded = false;

export default function Cards() {
  const{cards, loading} = useContext(CardsContext);
  const actions = useContext(DispatchContext);

  useEffect(() => {
    if(!isLoaded){
      isLoaded = true;
      actions.getCards();
    }
  }, [actions])
  
  function handleDeleteCard(id){
    actions.deleteCard(id)
  }

  return (
    <Container sx={{ flexGrow: 2}} >
      <Grid container>
        {loading === true ? 
        <LinearProgress /> :
        cards?.map(card => (
          <Grid key={card.id} item xs={3}>
            <CardDetail title={card.title} content={card.content} 
            buttonMessage={"Delete"} buttonAction={() => handleDeleteCard(card.id)} />
          </Grid>
        ))}
      </Grid>
    </Container>
  );
}