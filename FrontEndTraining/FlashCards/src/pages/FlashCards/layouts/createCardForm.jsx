import { useContext, useState } from 'react';
import ButtonP from '../../../components/Button/ButtonP';
import BasicInput from '../../../components/Input/Input';
import { DispatchContext } from '../../../context/cardsContext';
import Form from '../../../components/Form/Form';
import FormTitle from '../../../components/FormTitle/FormTitle';
import FormContent from '../../../components/FormContent/FormContent';
import FormActions from '../../../components/FormActions/FormActions';


export default function CreateCardForm({children}) {
  const [open, setOpen] = useState(false);
  const [card, setCard] = useState({title: '', content: ''});
  const [disabledButton, setDisabledButton] = useState(true);
  const actions = useContext(DispatchContext);

  const handleCreateButton = () => {
    actions.createCard(card);
    setOpen(false);
    setCard({title: '', content: ''});
  }

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };


  const validator = (value) => {
    let isValidate;
    if(value.split('').some(x => ['@', '$', '#', '_'].includes(x))) isValidate = false;
    else isValidate = true;
    return isValidate;
  }

  return (
    <div>
      <ButtonP action={handleClickOpen} sx={{margin: 2}} variant="contained">
        {children}
      </ButtonP>
      <Form open={open} onClose={handleClose}>
        <FormTitle>Create Card</FormTitle>
        <FormContent>
          <BasicInput
            errorInitital={false}
            margin="dense"
            id="Title"
            variant="standard"
            label="Title"
            setWord={setCard}
            word={card}
            name="title"
            validator={validator}
            setDisabledButton={setDisabledButton}
          />
          <BasicInput
            errorInitital={false}
            margin="dense"
            id="Content"
            label="Content"
            variant="standard"
            setWord={setCard}
            word={card}
            name="content"
            validator={validator}
            setDisabledButton={setDisabledButton}
          />
        </FormContent>
        <FormActions>
          <ButtonP action={handleClose}>Cancel</ButtonP>
          <ButtonP action={handleCreateButton} disabled={disabledButton}>Create</ButtonP>
        </FormActions>
      </Form>
    </div>
  );
}
