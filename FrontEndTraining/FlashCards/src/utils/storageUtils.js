function saveItems(key, items){
    localStorage.setItem(key, JSON.stringify(items));
}

function getItems(key){
    let items;
    if(localStorage.getItem(key) === null) items = [];
    else items = JSON.parse(localStorage.getItem(key));
    return items;
}

function saveItem(key, item){
    let items = getItems(key);
    items.push(item);
    saveItems(key, items);
}

function deleteItem(key, callback){
    let items = getItems(key);
    let newItems = items.filter(callback);
    saveItems(key, newItems);
}

export const storageUtils = {
    saveITems: (key, items) => saveItems(key, items),
    getItems: (key) => getItems(key),
    saveItem: (key, item) => saveItem(key, item),
    deleteItem: (key, callback) => deleteItem(key, callback)
}