import logo from './logo.svg';
import './App.css';
import Lista from './components/item';
import BasicCard from './components/card';
import Boton from './components/button';

function App() {

  const participants = ['Mateo', 'Marcos', 'Lucas', 'Juan'];

  return (
    <div className="App">
      <header className="App-header">
        <ul>
          {participants.map((participant, index) => 
            <Lista key={index} value={participant}></Lista>
          )}
        </ul>
        <BasicCard></BasicCard>
        <Boton warning/>
      </header>
    </div>
  );
}

export default App;
