import { Button } from "@mui/material";
import './style.css'

export default function Boton(props) {

    const {warning} = props;
  return (
    <Button className={warning && `danger`}>Press me</Button>
  )
}