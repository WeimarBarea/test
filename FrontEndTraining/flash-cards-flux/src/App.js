import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import FlashcardsPage from './pages/flashcards';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<FlashcardsPage/>}>

        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
