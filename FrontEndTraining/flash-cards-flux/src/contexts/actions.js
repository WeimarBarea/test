import { persistence } from "../helpers/persistence"
import { ActionTypes } from "./constants";

function getCards(dispatch) {
    dispatch({type: ActionTypes.LOADING_SWITCH, payload: true});
    persistence.getCardsAsync()
    .then((response) => dispatch({type: ActionTypes.CARD_LOAD, payload: response}))
    .finally(() => dispatch({type: ActionTypes.LOADING_SWITCH, payload: false}));
}

export default function ActionFactory(dispatch) {
    return {
        getCards: () => getCards(dispatch),
    }
}
