import { ActionTypes } from "./constants";

export function cardsReducer(state, action) {
  const {type, payload} = action;
  switch (type) {
    case ActionTypes.LOADING_SWITCH: {
      return {...state, loading: payload};
    }
    case ActionTypes.CARD_LOAD: {
      return {...state, cards: payload};
    }
    case ActionTypes.CARD_CREATE: {
      return {
        ...state,
        cards: [
        ...state.cards,
        {
          title: 'Title card ',
          content: 'Content card',
          topicId: '572269fd-50b6-43af-87e9-d21421d7234f'
        }
      ]};
    }
    case ActionTypes.CARD_DELETE: {
      return {...state, cards: state.cards.filter((card) => card.id !== payload)};
    }
    // case ActionTypes.CARD_UPDATE: {
    //   return cards.map((card) => {
    //     if (card.id === payload) {
    //       return {...card, title: 'updated' + (payload + 1)};
    //     } else {
    //       return card;
    //     }
    //   })
    // }
    default: {
      throw Error('Unknown action: ' + action.type);
    }
  }
}
