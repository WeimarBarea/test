const NAMESPACE = 'cards';

export const ActionTypes = {
    CARD_LOAD: `${NAMESPACE}.card.load`,
    CARD_CREATE: `${NAMESPACE}.card.create`,
    CARD_DELETE: `${NAMESPACE}.card.delete`,
    CARD_UPDATE: `${NAMESPACE}.card.update`,
    CARD_FILTER: `${NAMESPACE}.card.filter`,
    LOADING_SWITCH: `${NAMESPACE}.loading.switch`,
}