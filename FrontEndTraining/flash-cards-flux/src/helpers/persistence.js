import { storageUtils } from "../utils/storageUtils"
import initialCards from '../data/cards.json';
import { uuidv4 } from "../utils/uuid";

const CARDS_KEY = 'cards';

function getCardsAsync() {
    let cards = storageUtils.getItems(CARDS_KEY);
    if(cards.length === 0) {
        cards = initialCards;
        storageUtils.saveItems(CARDS_KEY, cards);
    }

    return new Promise((resolve, reject) => {
        if (cards.length === 0) {
            reject(new Error('Empty values.'))
        }
        setTimeout(() => {
            resolve(cards);
        }, 1500);
    })
}

function createCardAsync(card) {
    let uuid = uuidv4();
    card = {
        ...card,
        id: uuid,
        created: Date.now()
    }
    storageUtils.saveItem(CARDS_KEY, card)

    return new Promise((resolve, reject) => {
        if (!card.id) {
            reject(new Error('Error.'))
        }
        setTimeout(() => {
            resolve(card);
        }, 1500);
    })
}

function deleteCardAsync(cardId) {
    storageUtils.deleteItem(CARDS_KEY, ((card) => card.id !== cardId));

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(true);
        }, 1500);
    })
}

export const persistence = {
    getCardsAsync: () => getCardsAsync(),
    createCardAsync: (card) => createCardAsync(card),
    deleteCardAsync: (cardId) => deleteCardAsync(cardId)
}


/*
    get -> obtener resources GET
    update -> editar o modificar PATCH
    create -> crear un nuevo resource POST
    save -> crear o modificar un resource PUT
*/