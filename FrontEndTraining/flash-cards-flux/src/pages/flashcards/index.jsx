import React from 'react'
import { CardProvider } from '../../contexts/cardsContext'
import ActionBar from './layouts/actionBar'
import CardList from './layouts/cardList'

export default function FlashcardsPage() {
  return (
    <CardProvider>
      <ActionBar/>
      <CardList/>
    </CardProvider>
  )
}
