import React, { useContext } from "react";
import { DispatchContext } from "../../../contexts/cardsContext";

export default function ActionBar() {
  const actions = useContext(DispatchContext);

  function handleAddClick() {
    actions.getCards();
  }

  return (
    <div>
      <button onClick={handleAddClick}>Load cards</button>
    </div>
  )
}
