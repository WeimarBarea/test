import React, { useContext } from "react";
import { CardsContext } from "../../../contexts/cardsContext";

export default function CardList() {
  const {cards, loading} = useContext(CardsContext);
  return (
    <>
      {loading === true && <h1>Loading...</h1>}
      {cards.map(card => <h1 key={card.id}>{card.title + ' ' + card.id}</h1>)}
    </>
  );
}
