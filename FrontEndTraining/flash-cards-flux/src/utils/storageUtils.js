function saveItem(key, item) {
  let items = this.getItems(key);
  items.push(item);
  localStorage.setItem(key, JSON.stringify(items));
}

function getItems(key) {
  let items;
  if (localStorage.getItem(key) === null) {
    items = [];
  } else {
    items = JSON.parse(localStorage.getItem(key));
  }
  return items;
}

function saveItems(key, newItems) {
  localStorage.setItem(key, JSON.stringify(newItems));
}

function deleteItem(key, callback) {
  let items = this.getItems(key);
  items.filter(callback);
  localStorage.setItem(key, JSON.stringify(items));
}

export const storageUtils = {
    saveItem: (key, item) => saveItem(key, item),
    getItems: (key) => getItems(key),
    saveItems: (key, newItems) => saveItems(key, newItems),
    deleteItem: (key, callback) => deleteItem(key, callback)
}
