import './style.css'

function Button(props) {
    const {children, warning, onClick} = props;
    function handleClick(e) {
        e.stopPropagation();
        onClick(warning);
    }

    return <button className={warning ? 'danger': ''} onClick={handleClick}>{children}</button>
}

export default Button;
