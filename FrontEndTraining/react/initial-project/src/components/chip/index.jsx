import React from 'react'
import ChipMui from '@mui/material/Chip';

export default function Chip(props) {
    const {value} = props;
  return (
    <ChipMui label={value} />
  )
}
