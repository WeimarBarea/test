import ContainerMui from '@mui/material/Container';


export default function Container({children, maxWidth}) {
  return (
    <ContainerMui maxWidth={maxWidth}>
        {children}
    </ContainerMui>
  )
}

