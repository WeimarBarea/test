import React from 'react'

export default function item(props) {
    // prop {children, value}
    const {value} = props;
  return (
    <h4>{value}</h4>
  )
}
