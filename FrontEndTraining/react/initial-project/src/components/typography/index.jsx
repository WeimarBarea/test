import React from 'react'
import { Typography as TypographyMui } from '@mui/material'

export default function Typography({children, variant}) {
  return (
    <TypographyMui variant={variant}>{children}</TypographyMui>
  )
}
