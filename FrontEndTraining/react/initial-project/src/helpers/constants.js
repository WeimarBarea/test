const NAMESPACE = 'cards';

export const ActionTypes = {
    CARD_CREATE: `${NAMESPACE}.card.create`,
    CARD_DELETE: `${NAMESPACE}.card.delete`,
    CARD_UPDATE: `${NAMESPACE}.card.update`,
    CARD_FILTER: `${NAMESPACE}.card.update`,
}