import React from 'react'
import CardMui from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';

export default function Card(props) {
  const {content, actions, sx} = props;
  return (
    <CardMui sx={sx} raised elevation={2}>
      <CardContent>
        {content}      
      </CardContent>
      <CardActions>
        {actions}
      </CardActions>
    </CardMui>
  )
}
