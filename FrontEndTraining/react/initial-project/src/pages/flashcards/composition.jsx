import React from 'react'
import Button from '../../components/button'
import Typography from '../../components/typography';
import Card from './components/card';

export default function Flashcards() {

  return (
    <div >
      <Card content={
        <>
          <Typography>Hi</Typography>
          <Typography>team</Typography>
        </>
      }
      actions={
        <Button>Show more</Button>
      }
      />
    </div>
  )
}

