import { Typography } from '@mui/material';
import React, { useState } from 'react'
import Button from '../../components/button'

export default function Flashcards() {
  const [name, setName] = useState('');
  const [showError, setShowError] = useState(false);

  function handleClick(value) {
    console.log(value);
    setShowError(true);
  }

  function handleContainerClick(value) {
    console.log('container');
  }

  function handleChange(event) {
    setName(event.target.value)
    if(event.target.value.length > 6) {
      
      setShowError(true);
    }
  }

  return (
    <div onClick={handleContainerClick}>
      <h1> {name} </h1>
      <input type="text" onChange={handleChange} value={name} />
      {showError && <Typography style={{ color: 'red' }}>Alert Error</Typography>}
    </div>
  )
}

