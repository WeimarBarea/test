import React, { useState } from 'react'
import { useImmer } from "use-immer";
import Button from '../../components/button'
import Typography from '../../components/typography';
import Card from './components/card';

export default function Flashcards() {
  const [showModal, setShowModal] = useState(false);
  const [user, updateUser] = useImmer({name: 'Edith', lastName: 'Lopez', isSupervisor: true, emergencyContacts: [{name: 'Alvaro'}, {name:'Geo'}]});

  function handleClick() {
    updateUser(draft => {
      draft.name = 'Fernandez';
    });
  }

  return (
    <div >
      <Card content={
        <>
          <Typography>{user.name}</Typography>
          <Typography>{user.lastName}</Typography>
        </>
      }
      actions={
        <Button onClick={handleClick}>Show more</Button>
      }
      />
    </div>
  )
}

