import React, { createContext } from 'react'
import Container from '../../components/container';
import ViewCards from './layouts';

export const CardsContext = createContext(null);


export default function Flashcards() {
  return (
    <CardsContext.Provider value="title">
      <Container maxWidth="xl">
        <ViewCards/>
      </Container>
    </CardsContext.Provider>
  )
}
