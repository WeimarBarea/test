import React, { useContext } from "react";
import { CardsContext } from "..";
import Button from "../../../components/button";
import Typography from "../../../components/typography";
import Card from "../components/card";

export default function ViewCards() {
  const card = useContext(CardsContext);
  console.log(card);

  return (
    <div>
      <Card
        content={
          <>
            <Typography>Hi</Typography>
            <Typography>team</Typography>
          </>
        }
        actions={<Button>Show more</Button>}
      />
    </div>
  );
}
