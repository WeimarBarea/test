import React, { useReducer } from 'react'
import Button from '../../components/button'
import Container from '../../components/container';
import Typography from '../../components/typography';
import { ActionTypes } from '../../helpers/constants';
import { cardsReducer } from '../../reducers/cardsReducer';
import Card from './components/card';

export default function Flashcards() {
  const [cards, dispatch] = useReducer(cardsReducer, []);

  function handleAddClick() {
    dispatch({type: ActionTypes.CARD_CREATE});
  }

  function handleUpdateTask(cardId) {
    dispatch({type: ActionTypes.CARD_UPDATE, payload: cardId});
  }

  function handleDeleteTask(cardId) {
    dispatch({type: ActionTypes.CARD_DELETE, payload: cardId});
  }

  return (
    <Container maxWidth="xl">
      <Button onClick={handleAddClick}>Add Card</Button>
      {
        cards.map(card => 
        <Card key={card.id} sx={{ maxWidth: 328, backgroundColor: '#D6D6D6', mt:'16px'}} content={
          <>
            <Typography variant="h5">{card.title}</Typography>
            <Typography>{card.content}</Typography>
          </>
          }
          actions={
            <>
              <Button onClick={() => handleUpdateTask(card.id)}>Update</Button>
              <Button onClick={() => handleDeleteTask(card.id)}>Delete</Button>
            </>
          }
        />)
      }
      
    </Container>
  )
}
