import React, { useState } from 'react'
import Button from '../../components/button'
import Container from '../../components/container';
import Typography from '../../components/typography';
import Card from './components/card';

let nextId = 0;

export default function Flashcards() {
  const [cards, setCards] = useState([]);

  function handleAddClick() {
    // we cannot use push because is a mutable function
    // cards.push(new card)
    setCards(
      [
        ...cards,
        {
          id: nextId++,
          title: 'Title card' + (nextId),
          content: 'Content card',
        }
      ]
    );
  }

  function handleUpdateTask(cardId) {
    setCards(
      cards.map((card) => {
      if (card.id === cardId) {
        return {...card, title: 'updated' + (cardId + 1)};
      } else {
        return card;
      }
    }));
  }

  function handleDeleteTask(cardId) {
    setCards(cards.filter((card) => card.id !== cardId));
  }

  return (
    <Container maxWidth="xl">
      <Button onClick={handleAddClick}>Add Card</Button>
      {
        cards.map(card => 
        <Card key={card.id} sx={{ maxWidth: 328, backgroundColor: '#D6D6D6', mt:'16px'}} content={
          <>
            <Typography variant="h5">{card.title}</Typography>
            <Typography>{card.content}</Typography>
          </>
          }
          actions={
            <>
              <Button onClick={() => handleUpdateTask(card.id)}>Update</Button>
              <Button onClick={() => handleDeleteTask(card.id)}>Delete</Button>
            </>
          }
        />)
      }
      
    </Container>
  )
}

