import { ActionTypes } from "../helpers/constants";

let nextId = 0;

export function cardsReducer(cards, action) {
  const {type, payload} = action;
  switch (type) {
    case ActionTypes.CARD_CREATE: {
      return [
        ...cards,
        {
          id: nextId++,
          title: 'Title card' + (nextId),
          content: 'Content card',
        }
      ];
    }
    case ActionTypes.CARD_DELETE: {
      return cards.filter((card) => card.id !== payload)
    }
    case ActionTypes.CARD_UPDATE: {
      return cards.map((card) => {
        if (card.id === payload) {
          return {...card, title: 'updated' + (payload + 1)};
        } else {
          return card;
        }
      })
    }
    default: {
      throw Error('Unknown action: ' + action.type);
    }
  }
}
