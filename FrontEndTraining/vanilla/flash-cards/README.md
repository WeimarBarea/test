# Flashcards Project

This project has been created using **webpack-cli**, you can now run

```
npm run build
```

or

```
yarn build
```

to bundle your application

# Important data
> Node 18.13.0

Command to load the changes without build all time

```
npm run serve
```

Command to run linter

```
npm run lint
```
# Project info
## Models
``` javascript
Cards {
    id: guid,
    title: string,
    content: string,
    created: datetime
}
```
``` javascript
Topic {
    id: guid,
    name: string,
    description: string,
    cards: Cards[],
    created: datetime
}
```
``` javascript
Subject {
    id: guid,
    name: string,
    category: Enum Category,
    topics: Topic[],
    created: datetime
}
```
``` javascript
enum Category
{
    Backend,
    Frontend,
    English,
    Others,
}
```
## First Goal
> List subjects, topics and cards
- Performing the view to take the minimum number of actions
## Desing tip
### Spacing
pt = point

8pt

[spacing method](https://m2.material.io/design/layout/spacing-methods.html)

[Typography](https://gridlover.net/try.html)

## Utilities

[Guid generator](https://guidgenerator.com/online-guid-generator.aspx)

[Colors generator](https://coolors.co/)

