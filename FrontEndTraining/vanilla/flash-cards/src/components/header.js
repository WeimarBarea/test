export default function Header() {
    return (`
        <div id="header-container" class="center-container flex-row-container center">
            <h1 class="white">Flashcards</h1>
            <section class="flex-row-container gap2">
                <h4 class="white">Topics</h4>
                <h4 class="white">Subjects</h4>
            </section>
        </div>
    `);
}
