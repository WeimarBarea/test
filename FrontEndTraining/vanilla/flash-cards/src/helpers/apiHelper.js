import cardsData from '../data/cards.json'

export function getCards() {
    return cardsData;
}

export function getCardById(id) {
    return cardsData.find(x => x.id === id);
}
