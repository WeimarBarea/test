import { getCards } from "../helpers/apiHelper";

export default function Home() {
    const cards = getCards();
    return `
    <div class="cards-grid">
        ${cards.map(card =>
            `
            <section class="card">
                <div class="p3 flex-column-container card-container">
                    <h3>${card.title}</h3>
                    <h4 id="description">${card.content}</h4>
                    <h5>Subject</h5>
                    <div>
                        <span class="tooltip">Topic</span>
                    </div>
                </div>
            </section>
            `
        ).join("")}
    </div>
    `;
}
