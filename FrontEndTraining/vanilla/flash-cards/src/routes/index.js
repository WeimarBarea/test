import Header from "../components/header";
import Home from "../pages/home";
import Subjects from "../pages/subjects";
import Topics from "../pages/topics";

const routes = {
    '/': Home,
    '/subjects': Subjects,
    '/topics': Topics
}

export default async function router() {
    const header = null || document.getElementById('header');
    const content = null || document.getElementById('content');
    header.innerHTML = await Header();
    const {pathname} = window.location;
    const page = routes[pathname]
    content.innerHTML = await page();
}
