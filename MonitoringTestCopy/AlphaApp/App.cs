﻿using Core;
using Core.Entities;
using Programs;
using Programs.Commands;
using Views;

namespace AlphaApp
{
    public class App
    {
        public void Start()
        {
            List<IOptionView<IEntity>> views = new List<IOptionView<IEntity>>();

            Dictionary<IOptionView<IEntity>, ICommand> options = new Dictionary<IOptionView<IEntity>, ICommand>();

            IController<TrainingProgram> programController = new ProgramController();

            List<MenuOption> menuOptions = new List<MenuOption>();

            MenuOption CreateProgramOption = new MenuOption()
            {
                View = (IOptionView<IEntity>)new CreateProgramView(),
                Command = new RegisterProgramCommand(programController),
                OptionType = MenuOptionType.input
            };

            menuOptions.Add(CreateProgramOption);

            new MainMenu(menuOptions).Show();
        }

        
    }
}