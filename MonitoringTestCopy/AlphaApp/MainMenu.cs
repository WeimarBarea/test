﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Views;

namespace AlphaApp
{
    public class MainMenu
    {
        private List<IOptionView<IEntity>> views = new List<IOptionView<IEntity>>();
        private List<ICommand> commands = new List<ICommand>();

        public MainMenu(List<MenuOption> menuOptions)
        {
            foreach (var option in menuOptions)
            {
                this.views.Add(option.View);
                this.commands.Add(option.Command);
            }
        }

        public void Show()
        {
            this.ShowHeader();
            this.ShowOptions();
            this.ChooseOption();
        }

        private void ChooseOption()
        {
            Console.WriteLine("\nWrite the option number: ");
            int number = int.Parse(Console.ReadLine());


            this.views[number - 1].ShowContent();

            this.commands[number - 1].SetContext(this.views[number - 1].GetResult());

            try
            {
                this.commands[number - 1].Execute();
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error ocurred. {0}", ex.Message);
            }

            Console.WriteLine("Successful operation.");

            Show();
        }

        private void ShowHeader()
        {
            Console.WriteLine("=======================================");
            Console.WriteLine("          Dev Programs Budget          ");
            Console.WriteLine("=======================================");
        }

        private void ShowOptions()
        {
            if (views.Any())
            {
                int menuIndex = 1;
                foreach (IView view in views)
                {
                    Console.Write("{0}. ", menuIndex++.ToString());
                    view.Show();
                }
            }
            else
            {
                this.ShowNoOptions();
            }
        }

        private void ShowNoOptions()
        {
            Console.WriteLine("\n                No options registered                ");
        }
    }
}
