﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Views;

namespace AlphaApp
{
    public class MenuOption
    {
        public IOptionView<IEntity> View { get; set; }
        public ICommand Command { get; set; }
        public MenuOptionType OptionType { get; set; }
    }

    public enum MenuOptionType
    {
        input,
        output
    }
}
