﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface IEntity
    {
        string Name { get; set; }
        string Code { get; set; }
        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }
    }
}
