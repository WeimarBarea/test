﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface IRepository<T>
    {
        bool Save(T entity);
        IEnumerable<T> GetAll();
    }
}
