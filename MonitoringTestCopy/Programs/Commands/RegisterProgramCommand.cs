﻿using Core;
using Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programs.Commands
{
    public class RegisterProgramCommand : ICommand
    {
        private List<TrainingProgram> programs;
        private IController<TrainingProgram> progranController;

        public RegisterProgramCommand(IController<TrainingProgram> progranController)
        {
            this.progranController = progranController;
        }
        public void Execute()
        {
            this.progranController.Register(this.programs.First());
        }

        public void SetContext(IList<IEntity> entities)
        {
            this.programs = new List<TrainingProgram>();

            foreach (IEntity entity in entities)
            {
                this.programs.Add((TrainingProgram)entity);
            }
        }
    }
}
