﻿using Core;
using Core.Entities;
using Util;

namespace Programs
{
    public class ProgramController : IController<TrainingProgram>
    {
        private List<IProgramValidator> localValidators;
        IRepository<TrainingProgram> localRepository = new ProgramRepository();

        public ProgramController(IRepository<TrainingProgram>? repository, List<IProgramValidator>? validators)
        {
            if (repository == null)
                localRepository = new ProgramRepository();
            else
                localRepository = repository;

            if (validators != null)
                this.localValidators = validators;
        }

        public ProgramController(List<IProgramValidator>? validators = null) : this(null, validators)
        {

        }

        public ProgramController(IRepository<TrainingProgram>? repository) : this(repository, null)
        {

        }

        public bool Register(TrainingProgram item)
        {
            if(item == null) return false;
            return localRepository.Save(item);
        }

        public IEnumerable<TrainingProgram> Read()
        {
            return localRepository.GetAll();
        }

        public bool Validate(TrainingProgram item)
        {
            if (localValidators != null)
            {
                foreach (IProgramValidator validator in localValidators)
                {
                    if (!validator.Validate(item))
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}