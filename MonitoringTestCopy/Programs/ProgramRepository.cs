﻿using Core;
using Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programs
{
    public class ProgramRepository : IRepository<TrainingProgram>
    {
        private List<TrainingProgram> programs = new List<TrainingProgram>();
        public bool Save(TrainingProgram entity)
        {
            if (entity == null) return false;

            programs.Add(entity);
            return true;
        }

        public IEnumerable<TrainingProgram> GetAll()
        {
            return programs;
        }

    }
}
