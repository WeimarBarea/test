﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Util
{
    public interface IProgramValidator
    {
        bool Validate(TrainingProgram item);
    }
}
