﻿using Core;
using Core.Entities;

namespace Views
{
    public class CreateProgramView : IOptionView<IEntity>
    {
        List<TrainingProgram> result;

        public void Show()
        {
            Console.WriteLine("Register program");
        }

        public void ShowContent()
        {
            TrainingProgram newProgram = new TrainingProgram();
            Console.WriteLine("Enter the code: ");
            newProgram.Code = Console.ReadLine();
            Console.WriteLine("Enter the name: ");
            newProgram.Name = Console.ReadLine();
            Console.WriteLine("Enter the start date (dd/mm/yyyy): ");
            string inputDate = Console.ReadLine();
            string[] inputData = inputDate.Split('/');
            newProgram.StartDate = new DateTime(int.Parse(inputData[2]), int.Parse(inputData[1]), int.Parse(inputData[0]));
            Console.WriteLine("Enter the end date (dd/mm/yyyy): ");
            inputDate = Console.ReadLine();
            inputData = inputDate.Split('/');
            newProgram.EndDate = new DateTime(int.Parse(inputData[2]), int.Parse(inputData[1]), int.Parse(inputData[0]));

            this.result = new List<TrainingProgram>();
            this.result.Add(newProgram);
        }

        public IList<IEntity> GetResult()
        {
            List<IEntity> resultEntity = new List<IEntity>();
            
            foreach (var item in result)
            {
                resultEntity.Add(item);
            }

            return resultEntity;
        }
    }
}