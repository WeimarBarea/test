﻿using Controllers.Interfaces;
using Controllers.Validators;
using Models.Models;
using Repositories.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controllers.Controllers
{
    public class HolydayController : IController<HolidayModel>
    {
        public bool Add(HolidayModel holyday)
        {
            HolydayValidator validator = new HolydayValidator();
            bool valid = false;

            if (validator.IsValid(holyday))
            {
                HolydayRepository.Holydays.Add(holyday);
                valid = true;
            }

            return valid;
        }

        public IEnumerable<HolidayModel> GetAll()
        {
            return HolydayRepository.Holydays;
        }
    }
}
