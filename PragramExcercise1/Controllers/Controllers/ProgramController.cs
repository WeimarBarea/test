﻿using Controllers.Interfaces;
using Controllers.Validators;
using Models.Models;
using Repositories.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controllers.Controllers
{
    public class ProgramController : IController<ProgramModel>
    {
        ProgramValidator validator = new ProgramValidator();

        public bool Add(ProgramModel program)
        {
            bool isValid = false;

            if(validator.IsValid(program))
            {
                isValid = true;
                ProgramRepository.Programs.Add(program);
            }

            return isValid;
        }

        public IEnumerable<ProgramModel> GetAll()
        {
            return ProgramRepository.Programs;
        }

        public ProgramModel GetProgramByIndex(int index)
        {
            ProgramModel program;

            program = ProgramRepository.Programs[index - 1];

            return program;
        }
    }
}
