﻿using Controllers.Interfaces;
using Controllers.Validators;
using Models.Models;
using Repositories.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controllers.Controllers
{
    public class SubjectController : IController<SubjectModel>
    {
        SubjectValidator validator = new SubjectValidator();

        public bool Add(SubjectModel subject)
        {
            bool isValid = false;

            if (validator.isValid(subject))
            {
                SubjectRepository.Subjects.Add(subject);
                isValid= true;
            }

            return isValid;
        }

        public IEnumerable<SubjectModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SubjectModel> GetSubjectsByProgramCode(ProgramModel program)
        {
            IEnumerable<SubjectModel> subjects = SubjectRepository.Subjects; 
            List<SubjectModel> subjectsByProgramCode = new List<SubjectModel>();
            foreach (var item in subjects)
            {
                if(item.ProgramCode == program.Code)
                {
                    subjectsByProgramCode.Add(item);
                }
            }

            return subjectsByProgramCode;
        }
    }
}
