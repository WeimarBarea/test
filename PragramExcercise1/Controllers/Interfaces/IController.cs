﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controllers.Interfaces
{
    public interface IController<T>
    {
        bool Add(T obj);
        IEnumerable<T> GetAll();
    }
}
