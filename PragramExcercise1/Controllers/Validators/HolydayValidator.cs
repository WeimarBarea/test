﻿using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controllers.Validators
{
    public class HolydayValidator
    {
        public bool IsValid(HolidayModel holyday)
        {
            bool valid = true;

            if(holyday.Holyday.Year < 2023)
            {
                valid = false;
            }

            return valid;
        }
    }
}
