﻿using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controllers.Validators
{
    public class ProgramValidator
    {
        public bool IsValid(ProgramModel program)
        {
            bool valid = true;
            if (program.Code == string.Empty 
                || program.Code == null
                || program.StartDate.DayOfWeek != DayOfWeek.Monday
                || program.EndDate.DayOfWeek != DayOfWeek.Friday) 
            {
                valid= false;
            }

            if (program.Name == string.Empty || program.Name == null)
                program.Name = "[TBD]";

            return valid;
        }
    }
}
