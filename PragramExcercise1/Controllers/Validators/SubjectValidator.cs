﻿using Controllers.Controllers;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controllers.Validators
{
    public class SubjectValidator
    {
        public bool isValid(SubjectModel subject)
        {
            bool valid = true;
            if (subject.Sessions < 1 || subject.Sessions > 5
                || subject.Name == string.Empty || subject.Name == null
                || subject.HoursBySession != 1
                && subject.HoursBySession != 2
                && subject.HoursBySession != 0.5
                && subject.HoursBySession != 3
                && subject.HoursBySession != 1.5)
            {
                valid = false;
            }

            return valid;
        }
    }
}
