﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Controllers.Interfaces
{
    public interface IController<T>
    {
        bool Add(T obj);
    }
}
