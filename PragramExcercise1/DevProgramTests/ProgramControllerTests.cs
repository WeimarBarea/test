using Controllers.Controllers;
using Models.Models;

namespace DevProgramTests
{
    [TestClass]
    public class ProgramControllerTests
    {
        ProgramController controller = new ProgramController();
        ProgramModel program = new ProgramModel();

        [TestMethod]
        public void TestAddProgramWhenIsSuccessfulReturnstrue()
        {
            program.Name = "Test";
            program.Code= "Test";
            program.StartDate = DateTime.Parse("2023/2/6");
            program.EndDate = DateTime.Parse("2023/2/10");

            bool check = controller.Add(program);
            Assert.IsTrue(check);
        }
    }
}