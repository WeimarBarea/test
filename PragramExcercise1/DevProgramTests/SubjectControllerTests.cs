﻿using Controllers.Controllers;
using Models.Models;

namespace DevProgramTests
{
    [TestClass]
    public class SubjectControllerTests
    {
        SubjectController controller = new SubjectController();
        SubjectModel subject = new SubjectModel();

        [TestMethod]
        public void TestAddProgramWhenIsSuccessfulReturnstrue()
        {
            subject.Name = "Test";
            subject.Sessions = 5;
            subject.HoursBySession = 2;

            bool check = controller.Add(subject);
            Assert.IsTrue(check);
        }
    }
}