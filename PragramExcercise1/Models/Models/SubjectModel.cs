﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    public class SubjectModel
    {
        public string Name { get; set; }
        public int Sessions { get; set; }
        public double HoursBySession { get; set; }
        public string ProgramCode { get; set; }
        public string ClassDays { get; set; }
    }
}
