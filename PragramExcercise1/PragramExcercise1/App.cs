﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Views.Commands.Interfaces;
using Views.Commands.ProgramCommands;
using Views.MenuView;

namespace PragramExcercise1
{
    public class App
    {
        private MenuView menu;
        List<ICommand> commands;

        public App(MenuView menu)
        {
            this.menu = menu;
            commands = new List<ICommand>()
            {
                new RegisterProgramCommand(),
                new ShowAllProgramsCommand(),
                new RegisterSubjectCommand(),
                new ShowSubjectsByProgramCommand(),
                new RegisterHolydayCommand(),
                new ShowAllHolydaysCommand(),
                new GetSubjectsByProgramMinusHolydaysCommand(),
                new ClearScreenCommand(),
            };
        }

        public void Start()
        {
            int option;
            do
            {
                option = menu.ChooseOption();
                commands[option-1].Execute();
            }
            while (option != 0);
        }
    }
}
