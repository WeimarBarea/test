﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Views.Commands.Interfaces;

namespace Views.Commands.ProgramCommands
{
    public class ClearScreenCommand : ICommand
    {
        public void Execute()
        {
            Console.Clear();
        }
    }
}
