﻿using Controllers.Controllers;
using Models.Models;
using Repositories.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Views.Commands.Interfaces;
using Views.Helpers;
using Views.SubectView;

namespace Views.Commands.ProgramCommands
{
    public class GetSubjectsByProgramMinusHolydaysCommand : ICommand
    {
        public void Execute()
        {
            IEnumerable<ProgramModel> programs = ProgramRepository.Programs;
            GetSubjectsByProgramMinusHolydaysView view = new GetSubjectsByProgramMinusHolydaysView();
            ProgramController programController = new ProgramController();
            SubjectController subjectController = new SubjectController();
            ProgramDuration duration = new ProgramDuration();
            ProgramWithHolydays calculate = new ProgramWithHolydays();
            SubjectModel subject;
            ProgramModel program;

            if (programs.ToList().Count > 0)
            {
                int index = view.ShowAllPrograms(programs);
                program = programController.GetProgramByIndex(index);
                IEnumerable<SubjectModel> subjects = subjectController.GetSubjectsByProgramCode(program);
                List<int> holydaysInSessions = new List<int>();
                if (subjects.Count() > 0)
                {
                    holydaysInSessions = calculate.HolydaysInSessions(subjects, program);
                    int weeks = duration.WeekCalculator(program);
                    view.ShowAllSubjectsByProgram(subjects, program, weeks, holydaysInSessions);
                }
                else
                    view.Message("No subjects registered for this program");
            }
            else
                view.Message("No programs registered yet");
        }
    }
}
