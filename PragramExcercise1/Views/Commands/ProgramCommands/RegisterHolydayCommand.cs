﻿using Controllers.Controllers;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Views.Commands.Interfaces;
using Views.HolydayView;

namespace Views.Commands.ProgramCommands
{
    public class RegisterHolydayCommand : ICommand
    {
        RegisterHolydayView view = new RegisterHolydayView();
        public void Execute()
        {
            HolidayModel holyday = new HolidayModel();
            InputData input = new InputData();
            HolydayController holydayController = new HolydayController();

            input = view.ShowRegisterHolydayView();
            holyday = CreateHolyday(input);
            bool check = holydayController.Add(holyday);

            string message = check ? $"Holyday: {holyday.Holyday.ToString("yyyy/MM/dd")} was successfully registered" :
                "An error ocurred during ragistration";

            view.ShowMessage(message);
        }

        public HolidayModel CreateHolyday(InputData data)
        {
            HolidayModel holyday = new HolidayModel();

            holyday.Holyday = Convert.ToDateTime(data.Input["HolydayName"]);

            return holyday;
        }
    }
}
