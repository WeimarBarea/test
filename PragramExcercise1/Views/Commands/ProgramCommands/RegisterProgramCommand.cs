﻿using Controllers.Controllers;
using Controllers.Interfaces;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Views.Commands.Interfaces;
using Views.ProgramView;

namespace Views.Commands.ProgramCommands
{
    public class RegisterProgramCommand : ICommand
    {
        public void Execute()
        {
            AddProgramView view = new AddProgramView();
            IController<ProgramModel> programController = new ProgramController();
            InputData inputData = view.ShowAddProgramView();

            ProgramModel program = CreateProgram(inputData);
            bool result = programController.Add(program);

            string message = result == true ? $"Program {program.Name} succesfully created" : 
                "Program not registered due an input error";

            view.ShowMessage(message);
        }

        public ProgramModel CreateProgram(InputData inputData)
        {
            ProgramModel program = new ProgramModel();

            program.Code = inputData.Input["Code"];
            program.Name= inputData.Input["Name"];
            program.StartDate = DateTime.Parse(inputData.Input["StartDate"]);
            program.EndDate = DateTime.Parse(inputData.Input["EndDate"]);

            return program;
        }
    }
}
