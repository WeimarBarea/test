﻿using Controllers.Controllers;
using Controllers.Interfaces;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Views.Commands.Interfaces;
using Views.Helpers;
using Views.SubectView;

namespace Views.Commands.ProgramCommands
{
    public class RegisterSubjectCommand : ICommand
    {
        public void Execute()
        {
            IController<ProgramModel> programController = new ProgramController();
            IController<SubjectModel> subjectController = new SubjectController();   
            List<ProgramModel> programs = programController.GetAll().ToList();
            AddSubjectView view= new AddSubjectView();

            if(programs.ToList().Count > 0)
            {
                int index = view.ShowAllPragrams(programs);
                InputData inputData = view.ShowAddSubjectView();

                SubjectModel subject = CreateSubject(inputData, programs[index - 1].Code);
                bool result = subjectController.Add(subject);

                string message = result == true ? $"The subject: {subject.Name} was succesfully created" :
                    $"Subject not registered due an input error";

                view.ShowMessage(message);

            }
            else
            {
                string message = "No programs registered yet...";
                view.ShowMessage(message);
            }
        }

        public SubjectModel CreateSubject(InputData input, string code)
        {
            SubjectModel subject = new SubjectModel();
            CountDays count = new CountDays();


            subject.Name = input.Input["SubjectName"];
            int sessions = count.Count(input.Input["Sessions"]);
            subject.Sessions = sessions;
            subject.ClassDays = input.Input["Sessions"];
            subject.HoursBySession = Convert.ToDouble(input.Input["Hours"], CultureInfo.InvariantCulture);
            subject.ProgramCode = code;

            return subject;
        }
    }
}
