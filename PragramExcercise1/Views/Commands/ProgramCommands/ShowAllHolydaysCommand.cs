﻿using Models.Models;
using Repositories.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Views.Commands.Interfaces;
using Views.HolydayView;

namespace Views.Commands.ProgramCommands
{
    public class ShowAllHolydaysCommand : ICommand
    {
        public void Execute()
        {
            IEnumerable<HolidayModel> holydays = HolydayRepository.Holydays;
            ShowAllHolydaysView view = new ShowAllHolydaysView();

            if (holydays.Count() > 0)
            {
                view.ShowHollydays(holydays);
            }
            else
                view.ShowErrorMessage();
        }
    }
}
