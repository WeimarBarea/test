﻿using Controllers.Controllers;
using Controllers.Interfaces;
using Models.Models;
using Repositories.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Views.Commands.Interfaces;
using Views.Helpers;
using Views.ProgramView;

namespace Views.Commands.ProgramCommands
{
    public class ShowAllProgramsCommand : ICommand
    {
        public void Execute()
        {
            GetAllProgramsView getAllProgramsView = new GetAllProgramsView();
            ProgramDuration duration = new ProgramDuration();
            IController<ProgramModel> programController = new ProgramController();
            IEnumerable<ProgramModel> programs = programController.GetAll();

            if (programs.Count() > 0)
            {
                foreach (ProgramModel program in programs)
                {
                    int weeks = duration.WeekCalculator(program);
                    getAllProgramsView.ShowAllPrograms(program, weeks);
                }
            }

            else
                getAllProgramsView.ShowErrorMessage();
        }
    }
}
