﻿using Controllers.Controllers;
using Models.Models;
using Repositories.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Views.Commands.Interfaces;
using Views.Helpers;
using Views.SubectView;

namespace Views.Commands.ProgramCommands
{
    public class ShowSubjectsByProgramCommand : ICommand
    {
        public void Execute()
        {
            IEnumerable<ProgramModel> programs = ProgramRepository.Programs;
            GetAllSubjectsByProgramView view = new GetAllSubjectsByProgramView();
            ProgramController programController = new ProgramController();
            SubjectController subjectController = new SubjectController();
            ProgramDuration duration= new ProgramDuration();
            SubjectModel subject = new SubjectModel();
            ProgramModel program = new ProgramModel();

            if (programs.ToList().Count > 0)
            {
                int index = view.ShowAllPrograms(programs);
                program = programController.GetProgramByIndex(index);
                IEnumerable<SubjectModel> subjects = subjectController.GetSubjectsByProgramCode(program);
                if (subjects.Count() > 0)
                {
                    int weeks = duration.WeekCalculator(program);
                    view.ShowAllSubjectsByProgram(subjects, program, weeks);
                }
                else
                    view.Message("No subjects registered for this program");
            }
            else
                view.Message("No programs registered yet");
        }
    }
}
