﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Views.Helpers
{
    public class CountDays
    {
        public int Count(string ClassDays)
        {
            int days;

            if (ClassDays.ToLower() == "all")
                days = 5;
            else
            {
                string[] ArrayClassDays = ClassDays.Split(',');
                days = ArrayClassDays.Length;
            }
            
            return days;
        }
    }
}
