﻿using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Views.Helpers
{
    public class GetAllPrograms
    {
        public int AllPragrams(IEnumerable<ProgramModel> programs)
        {
            int option;

            int cont = 1;

            Console.WriteLine("Select a Program");

            foreach (ProgramModel item in programs)
            {
                Console.WriteLine($"{cont}. Code: {item.Code} Name: {item.Name}");
                cont++;
            }

            return Convert.ToInt32(Console.ReadLine());
        }

    }
}
