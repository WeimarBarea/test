﻿using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Views.Helpers
{
    public class ProgramDuration
    {
        public int WeekCalculator(ProgramModel program)
        {
            int count = 1;

            DateTime start = program.StartDate;

            TimeSpan days = program.EndDate.Subtract(program.StartDate);

            for (int i = 0; i < days.Days; i++)
            {
                if(start.DayOfWeek != DayOfWeek.Saturday || start.DayOfWeek != DayOfWeek.Sunday)
                    count++;

                start = start.AddDays(1);
            }

            return count/5;
            
        }
    }
}
