﻿using Models.Models;
using Repositories.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Views.Helpers
{
    public class ProgramWithHolydays
    {
        public int Calculator(ProgramModel program, SubjectModel subject)
        {
            DateTime start = program.StartDate;
            TimeSpan weekdays = program.EndDate.Subtract(program.StartDate);
            List<HolidayModel> holidays = HolydayRepository.Holydays;

            List<DayOfWeek> ClassDays = new List<DayOfWeek>();
            int count = 0;


            if (subject.ClassDays != "all")
            {
                string[] days = subject.ClassDays.Split(',');
                foreach (string day in days)
                {
                    if (day == "mo")
                    {
                        ClassDays.Add(DayOfWeek.Monday);
                    }
                    if (day == "tu")
                    {
                        ClassDays.Add(DayOfWeek.Tuesday);
                    }
                    if (day == "we")
                    {
                        ClassDays.Add(DayOfWeek.Wednesday);
                    }
                    if (day == "th")
                    {
                        ClassDays.Add(DayOfWeek.Thursday);
                    }
                    if (day == "fr")
                    {
                        ClassDays.Add(DayOfWeek.Friday);
                    }
                }
            }
            else
            {
                ClassDays.Add(DayOfWeek.Monday);
                ClassDays.Add(DayOfWeek.Tuesday);
                ClassDays.Add(DayOfWeek.Wednesday);
                ClassDays.Add(DayOfWeek.Thursday);
                ClassDays.Add(DayOfWeek.Friday);
            }

            foreach (var day in ClassDays)
            {
                foreach (var holyday in holidays)
                {
                    if(day == holyday.Holyday.DayOfWeek) 
                        count++;
                }
            }

            return count;
        }

        public List<int> HolydaysInSessions(IEnumerable<SubjectModel> subjects, ProgramModel program)
        {
            List<int> days = new List<int>();

            foreach (var item in subjects)
            {
                days.Add(Calculator(program, item));
            }

            return days;
        }
    }
}
