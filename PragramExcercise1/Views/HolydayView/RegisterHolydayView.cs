﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Views.HolydayView
{
    public class RegisterHolydayView
    {
        public InputData ShowRegisterHolydayView()
        {
            InputData input = new InputData();
            Console.WriteLine("Enter the holyday in the next format: YYYY/MM/dd");
            string date = Console.ReadLine();
            input.Input.Add("HolydayName", date);

            return input;
        }

        public void ShowMessage(string message)
        {
            Console.WriteLine($"{message}\n");
        }
    }
}
