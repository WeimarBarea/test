﻿using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Views.HolydayView
{
    public class ShowAllHolydaysView
    {
        public void ShowHollydays(IEnumerable<HolidayModel> holydays)
        {
            Console.WriteLine("Holyday date     Holyday day");
            Console.WriteLine("----------------------------");
            foreach (var holiday in holydays)
            {
                Console.WriteLine($"{holiday.Holyday.ToString("yyyy/MM/dd")}     {holiday.Holyday.DayOfWeek}");
            }
        }

        public void ShowErrorMessage()
        {
            Console.WriteLine("No holyday registered yet \n");
        }
    }
}
