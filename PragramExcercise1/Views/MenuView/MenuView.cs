﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Views.MenuView
{
    public class MenuView
    {
        public int ChooseOption()
        {
            ShowMenu();
            return Convert.ToInt32(Console.ReadLine());
        }

        public void ShowMenu()
        {
            Console.WriteLine("Welcome");
            Console.WriteLine("Please select an option");
            Console.WriteLine("---------------------------------------");
            Console.WriteLine("1: Register program");
            Console.WriteLine("2: Show all programs");
            Console.WriteLine("3. Register subjects");
            Console.WriteLine("4. Show all subjects by program");
            Console.WriteLine("5. Register holydays");
            Console.WriteLine("6. Show all holydays");
            Console.WriteLine("7. Show all subjects by program taking into consideration the holydays");
            Console.WriteLine("8. Clear screen");
            Console.WriteLine("0: Exit");
        }
    }
}
