﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Views.ProgramView
{
    public class AddProgramView
    {
        public InputData ShowAddProgramView()
        {
            InputData inputData = new InputData();

            Console.WriteLine("Enter the program's code");
            inputData.Input.Add("Code", Console.ReadLine());
            Console.WriteLine("Enter the program's name(optional)");
            inputData.Input.Add("Name", Console.ReadLine());
            Console.WriteLine("Enter the program's start date (it must be on monday)");
            inputData.Input.Add("StartDate", Console.ReadLine());
            Console.WriteLine("Enter the program's end date (it must be on friday)");
            inputData.Input.Add("EndDate", Console.ReadLine());

            return inputData;
        }

        public void ShowMessage(string message)
        {
            Console.WriteLine($"{message} \n");
        }
    }
}
