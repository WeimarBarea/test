﻿using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Views.ProgramView
{
    public class GetAllProgramsView
    {
        public void ShowAllPrograms(ProgramModel program, int duration)
        {
            Console.WriteLine("-------------------------------------------------------------------------------------------");
            Console.WriteLine("Code          Name            Start date             End date              Duration (weeks)");
            Console.WriteLine($"{program.Code}           {program.Name}             {program.StartDate.ToString("yyyy/MM/dd")}            {program.EndDate.ToString("yyyy/MM/dd")}              {duration}");
            Console.WriteLine("-------------------------------------------------------------------------------------------");
        }

        public void ShowErrorMessage()
        {
            Console.WriteLine("No programs registered yet \n");
        }
    }
}
