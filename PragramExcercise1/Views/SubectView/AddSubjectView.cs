﻿using Models.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Views.Helpers;
using Views.ProgramView;

namespace Views.SubectView
{
    public class AddSubjectView
    {
        public int ShowAllPragrams(IEnumerable<ProgramModel> programs)
        {
            GetAllPrograms getAll = new GetAllPrograms();
            int option = getAll.AllPragrams(programs);

            return option;
        }

        public InputData ShowAddSubjectView()
        {
            InputData data = new InputData();

            Console.WriteLine("Enter the subject's name");
            data.Input.Add("SubjectName", Console.ReadLine());
            Console.WriteLine("Enter the class days in the next format: Mo,tu,we,th,fr. If you want all the days type dawn: All");
            data.Input.Add("Sessions", Console.ReadLine());
            Console.WriteLine("Enter the number of hours per session");
            data.Input.Add("Hours", Console.ReadLine());

            return data;    
        }

        public void ShowMessage(string message)
        {
            Console.WriteLine($"{message} \n");
        }
    }
}
