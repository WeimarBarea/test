﻿using Models.Models;
using Repositories.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Views.Helpers;

namespace Views.SubectView
{
    public class GetAllSubjectsByProgramView
    {
        public int ShowAllPrograms(IEnumerable<ProgramModel> programs)
        {
            GetAllPrograms getAll = new GetAllPrograms();
            int option = getAll.AllPragrams(programs);
            return option;
        }

        public void ShowAllSubjectsByProgram(IEnumerable<SubjectModel> subjects, ProgramModel program, int duration)
        {
            Console.WriteLine("---------------------------------------------------");
            Console.WriteLine($"Code: {program.Code} Name: {program.Name} Duration:{duration}");
            Console.WriteLine("---------------------------------------------------");
            foreach (var item in subjects)
            {
                Console.WriteLine($"Subject Name: {item.Name} Sessions: {item.Sessions * duration} Total Hours: {item.Sessions * duration * item.HoursBySession}");
            }
            Console.WriteLine("\n");
        }

        public void Message(string message)
        {
            Console.WriteLine($"{message}\n");
        }
    }
}
