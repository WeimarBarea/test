﻿using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Views.Helpers;

namespace Views.SubectView
{
    public class GetSubjectsByProgramMinusHolydaysView
    {
        public int ShowAllPrograms(IEnumerable<ProgramModel> programs)
        {
            GetAllPrograms getAll = new GetAllPrograms();
            int option = getAll.AllPragrams(programs);
            return option;
        }

        public void ShowAllSubjectsByProgram(IEnumerable<SubjectModel> subjects, ProgramModel program, int duration, List<int> holidaysInSessions)
        {
            int index = 0;
            Console.WriteLine("---------------------------------------------------");
            Console.WriteLine($"Code: {program.Code} Name: {program.Name} Duration:{duration}");
            Console.WriteLine("---------------------------------------------------");
            foreach (var item in subjects)
            {
                Console.WriteLine($"Subject Name: {item.Name} Effective sessions days: {(item.Sessions * duration) - holidaysInSessions[index]} Total effective Hours: {((item.Sessions * duration) - holidaysInSessions[index]) * item.HoursBySession}");
            }
            Console.WriteLine("\n");
        }

        public void Message(string message)
        {
            Console.WriteLine($"{message}\n");
        }
    }
}
