﻿using JalaU.Budget.Core;
using JalaU.Budget.Core.Entities;
using JalaU.Budget.Programs;
using JalaU.Budget.Programs.Commands;
using JalaU.Budget.Views;

namespace JalaU.Budget.AlphaApp
{
    public class AlphaApp
    {
        public void Start()
        {
            List<IOptionView<IEntity>> views = new List<IOptionView<IEntity>>();

            IController<TrainingProgram> programController = new ProgramsController();

            IController<Subject> subjectController = new SubjectController();

            List<MenuOption> menuOptions = new List<MenuOption>();

            MenuOption CloseApplication = new MenuOption()
            {
                View = new CloseApp(),
            };
            menuOptions.Add(CloseApplication);

            MenuOption CreateProgramOption = new MenuOption()
            {
                View = new CreateProgramView(),
                Command = new RegisterProgramCommand(programController),
                optionType = MenuOptionType.Input
            };
            menuOptions.Add(CreateProgramOption);

            MenuOption ListProgramsOption = new MenuOption()
            {
                View = new ListProgramsView(),
                Command = new ListProgramsCommand(programController),
                optionType = MenuOptionType.Output
            };
            menuOptions.Add(ListProgramsOption);

            MenuOption CreateSubjectOption = new MenuOption()
            {
                View = new CreateSubjectView(),
                Command = new RegisterSubjectCommand(subjectController),
                optionType = MenuOptionType.Input
            };
            menuOptions.Add(CreateSubjectOption);

            MenuOption DurationOption = new MenuOption()
            {
                View = new CalculateDurationView(),
                Command = new CalculateDurationCommand(programController),
                optionType = MenuOptionType.Extra
            };
            menuOptions.Add(DurationOption);

            new MainMenu(menuOptions).Show();
        }
    }
}