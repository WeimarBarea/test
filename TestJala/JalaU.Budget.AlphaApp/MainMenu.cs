﻿using JalaU.Budget.Core;
using JalaU.Budget.Views;

namespace JalaU.Budget.AlphaApp
{
    public class MainMenu
    {
        private List<IOptionView<IEntity>> views= new List<IOptionView<IEntity>>();
        private List<IBudgetCommand> commands = new List<IBudgetCommand>();
        private List<MenuOptionType> options = new List<MenuOptionType>();

        public MainMenu(List<MenuOption> menuOptions)
        {
            foreach(var option in menuOptions)
            {
                this.views.Add(option.View);
                this.commands.Add(option.Command);
                this.options.Add(option.optionType);
            }
        }
        
        public void Show()
        {
            this.ShowHeader();
            this.ShowOptions();
            this.ChooseOption();
        }

        private void ShowHeader()
        {
            Console.WriteLine("===========================================");
            Console.WriteLine("            Dev Programs Budget            ");
            Console.WriteLine("===========================================");
        }

        private void ShowOptions()
        {
            if (views.Any())
            {
                int menuIndex = 0;
                foreach (IView view in views)
                {
                    Console.Write("{0}. ", menuIndex++.ToString());
                    view.Show();
                }
            }            
            else
            {
                this.ShowNoOptions();
            }
        }

        private void ChooseOption()
        {
            Console.Write("\nWrite the option number: ");
            int number = int.Parse(Console.ReadLine());

            if(number == 0)
            {
                return;
            }

            if(this.options[number] == MenuOptionType.Input)
            {
                ExecuteInputOption(number + 1);
            }
            if (this.options[number] == MenuOptionType.Output)
            {
                ExecuteOutputOption(number + 1);
            }
            if (this.options[number] == MenuOptionType.Extra)
            {
                ExecuteExtraOption(number + 1);
            }

            Console.WriteLine("Successful operation.");

            this.Show();
        }

        private void ExecuteExtraOption(int number)
        {
            try
            {
                this.commands[number - 1].Execute();
            }
            catch(Exception ex)
            {
                Console.WriteLine("An error occurred. {0}", ex.Message);
            }

            ((IOutputView)this.views[number - 1]).SetData(((IOutputCommand)this.commands[number - 1]).GetContext());

            this.views[number - 1].ShowContent();
        }

        private void ExecuteOutputOption(int number)
        {
            try
            {
                this.commands[number - 1].Execute();
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred. {0}", ex.Message);
            }

            ((IOutputView)this.views[number - 1]).SetData(((IOutputCommand)this.commands[number - 1]).GetContext());

            this.views[number - 1].ShowContent();
        }

        private void ExecuteInputOption(int number)
        {
            this.views[number - 1].ShowContent();

            ((IInputCommand)this.commands[number - 1]).SetContext(((IInputView)this.views[number - 1]).GetData());

            try
            {
                this.commands[number - 1].Execute();
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred. {0}", ex.Message);
            }
        }

        private void ShowNoOptions()
        {
            Console.WriteLine("\n           No options registered            ");
        }
    }
}