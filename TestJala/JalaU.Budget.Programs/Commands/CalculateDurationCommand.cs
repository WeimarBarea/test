﻿using JalaU.Budget.Core;
using JalaU.Budget.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JalaU.Budget.Programs.Commands
{
    public class CalculateDurationCommand : IOutputCommand
    {
        private IController<TrainingProgram> programController;
        private IEnumerable<TrainingProgram> programs;

        public CalculateDurationCommand(IController<TrainingProgram> programController)
        {
            this.programController = programController;
        }

        public void Execute()
        {
            this.programs = programController.Read();
        }

        private void Calculate()
        {
            double total = 0;
            foreach (var programModel in programs)
            {
                TimeSpan Diff = programModel.EndDate - programModel.StartDate;
                double totalWeeks = ((Diff.TotalDays) - (Diff.TotalDays % 7)) / 7;
                programModel.Duration = totalWeeks + 1;

                Console.WriteLine(programModel.Duration);
            }
        }

        public List<IEntity> GetContext()
        {
            Calculate();
            return this.programs.ToList<IEntity>();
        }
    }
}
