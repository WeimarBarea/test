﻿using JalaU.Budget.Core;
using JalaU.Budget.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JalaU.Budget.Programs.Commands
{
    public class RegisterSubjectCommand : IInputCommand
    {
        public List<Subject> subjects;
        private IController<Subject> subjectController;

        public RegisterSubjectCommand(IController<Subject> programController)
        {
            this.subjectController = programController;
        }

        public void Execute()
        {
            this.subjectController.Register(this.subjects.First());
        }

        public void SetContext(List<IEntity> entities)
        {
            this.subjects = new List<Subject>();
            foreach (IEntity entity in entities)
            {
                this.subjects.Add((Subject)entity);
            }
        }
    }
}
