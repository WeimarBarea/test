﻿using JalaU.Budget.Core.Entities;
using JalaU.Budget.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JalaU.Budget.Util.Validators;

namespace JalaU.Budget.Programs
{
    public class SubjectController : IController<Subject>
    {
        private List<ISubjectValidator> localValidators;
        IRepository<Subject> localRepository;

        public SubjectController(IRepository<Subject>? repository, List<ISubjectValidator>? validators = null)
        {
            if (repository == null)
                localRepository = new SubjectRepository();
            else
                localRepository = repository;

            if (validators != null)
                this.localValidators = validators;
        }

        public SubjectController(List<ISubjectValidator>? validators = null) : this(null, validators)
        {

        }

        public SubjectController(IRepository<Subject>? repository) : this(repository, null)
        {

        }

        public bool Register(Subject item)
        {
            if (!this.Validate(item))
            {
                return false;
            }

            return localRepository.Save(item);
        }

        private bool Validate(Subject item)
        {
            if (localValidators != null)
            {
                foreach (ISubjectValidator validator in localValidators)
                {
                    if (!validator.Validate(item))
                    {
                        return false;
                    }
                }
            }

            return true;
        }
        public IEnumerable<Subject> Read()
        {
            throw new NotImplementedException();
        }
    }
}
