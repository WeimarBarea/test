﻿using JalaU.Budget.Core;
using JalaU.Budget.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JalaU.Budget.Programs
{
    public class SubjectRepository : IRepository<Subject>
    {
        private List<Subject> _subjects = new List<Subject>();
        public IEnumerable<Subject> GetAll()
        {
            return _subjects;
        }

        public bool Save(Subject entity)
        {
            _subjects.Add(entity);
            return true;
        }
    }
}
