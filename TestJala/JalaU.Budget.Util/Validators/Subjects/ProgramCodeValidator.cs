﻿using JalaU.Budget.Core;
using JalaU.Budget.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JalaU.Budget.Util.Validators.Subjects
{
    public class EmptyProgramCodeValidator : ISubjectValidator
    {
        public bool Validate(Subject item)
        {
            return item.ProgramCode != string.Empty;            
        }
    }

    public class LengthProgramCodeValidator : ISubjectValidator
    {
        public bool Validate(Subject item)
        {
            return item.ProgramCode.Length >= 3;
        }
    }
}
