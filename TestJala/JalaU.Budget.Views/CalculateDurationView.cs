﻿using JalaU.Budget.Core;
using JalaU.Budget.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JalaU.Budget.Views
{
    public class CalculateDurationView : IOutputView
    {
        private List<TrainingProgram> programs;
        private double duration;
        
        public void Show()
        {
            Console.WriteLine("Calculate Duration");
        }

        public void ShowContent()
        {
            Console.WriteLine("Program Duration: {0} ", duration);
        }

        public void SetData(List<IEntity> items)
        {
            

            this.programs = new List<TrainingProgram>();
            foreach (var item in items)
            {
                programs.Add((TrainingProgram)item);
            }

            foreach (var item in programs)
            {
                duration += item.Duration;
            }
        }
    }
}
