﻿using JalaU.Budget.Core.Entities;
using JalaU.Budget.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JalaU.Budget.Views
{
    public class CreateSubjectView : IInputView
    {
        List<Subject> result;

        public List<IEntity> GetData()
        {
            List<IEntity> resultAsEntity = new List<IEntity>();
            foreach (var item in this.result)
            {
                resultAsEntity.Add((IEntity)item);
            }
            return resultAsEntity;
        }

        public void Show()
        {
            Console.WriteLine("Register a subject");
        }

        public void ShowContent()
        {
            Subject newSubject = new Subject();
            Console.Write("Enter the program's code: ");
            newSubject.ProgramCode = Console.ReadLine();
            Console.Write("Enter the name: ");
            newSubject.Name = Console.ReadLine();

            this.result = new List<Subject>();
            this.result.Add(newSubject);
        }
    }
}
