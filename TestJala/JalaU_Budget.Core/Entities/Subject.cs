﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JalaU.Budget.Core.Entities
{
    public class Subject : IEntity
    {
        public string ProgramCode { get; set; }
        public string Name { get; set; }
        Guid IEntity.Id => Guid.Empty;
    }
}
