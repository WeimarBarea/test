﻿using JalaU.Budget.Core;
using JalaU.Budget.Core.Entities;
using JalaU.Budget.Programs;
using JalaU.Budget.Util.Validators;
using JalaU.Budget.Util.Validators.Programs;
using JalaU.Budget.Util.Validators.Subjects;

namespace JalaU_Budget.Programs.Tests
{
    [TestClass]
    public class SubjectTests
    {
        [TestMethod]
        public void TestSubjectControllerWheRegisterIsSuccessful()
        {
            IController<Subject> myController = new SubjectController();
            Subject newSubject = new Subject();

            newSubject.ProgramCode = "Dev38";
            newSubject.Name = "Java";
            

            bool result = myController.Register(newSubject);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestSubjectontrollerWheProgramCodeIsEmpty()
        {
            List<ISubjectValidator> validators = new List<ISubjectValidator>
            {
                new EmptyProgramCodeValidator()
            };

            IController<Subject> myController = new SubjectController(validators);
            Subject newSubject = new Subject();

            newSubject.ProgramCode = string.Empty;
            newSubject.Name = "Java";
          

            bool result = myController.Register(newSubject);

            Assert.IsFalse(result);
        }

        
    }
}