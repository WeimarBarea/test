﻿using Data;
using Data.Entities;
using Presentation;
using Presentation.View;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Commands
{
    class SavePayment
    {
        public void Execute()
        {
            InputData data = new InputData();
            Member member;
            Consumption consumption = new Consumption();
            SavePaymentView view = new SavePaymentView();
            data = view.RequestData();
            member = new MemberRepository().GetMember(Convert.ToInt32(data.fields["CodigoSocio"]));
            if (member != null )
            {
                List<Consumption> consumptions = new ConsumptionRepository().GetConsumptionByMember(member);
                if (consumptions.Count > 0)
                {
                    view.ShowConsumptions(consumptions);
                }

                else
                    view.Message("No consumption registered yet");
            }

            else
            {
                view.Message("No members with that code");
            }

        }
    }
}
