﻿using Data;
using Data.Entities;
using Presentation;
using Presentation.View;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Commands
{
    class SaveUser
    {
        public void Execute()
        {
            InputData data = new SaveUserView().RequestData();
            Member member = new Member();
            member.ID = Convert.ToInt32(data.fields["CodigoSocio"]);
            member.FirstName = data.fields["Nombre"];
            member.SecondName = data.fields["Apellido"];
            new MemberRepository().Save(member);
        }
    }
}
