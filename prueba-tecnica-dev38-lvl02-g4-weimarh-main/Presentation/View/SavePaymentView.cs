﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Presentation.View
{
    public class SavePaymentView
    {
        public InputData RequestData()
        {
            return this.Show();
        }

        private InputData Show()
        {
            InputData input = new InputData();
            Console.WriteLine("======================================");
            Console.WriteLine("1. Ingrese el codigo del socio: ");
            input.fields.Add("CodigoSocio", Console.ReadLine());
            return input;
        }

        public void Message(string message)
        {
            Console.WriteLine(message);
        }

        public void ShowConsumptions(List<Consumption> consumptions)
        {
            foreach (var item in consumptions)
            {
                Console.WriteLine(item.Value);
                item.Paid = true;
            }
        }
    }
}
