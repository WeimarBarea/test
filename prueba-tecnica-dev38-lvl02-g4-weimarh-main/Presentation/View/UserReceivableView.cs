﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Presentation.View
{
    public class UserReceivableView
    {
        public InputData RequestData()
        {
            return this.GetData();
        }

        private InputData GetData()
        {
            InputData input = new InputData();
            Console.WriteLine("======================================");
            Console.WriteLine("1. Ingrese el codigo del socio: ");
            input.fields.Add("CodigoSocio", Console.ReadLine());
            return input;
        }

        public void ShowResult(double total, Member member, int totalConsumption)
        {            
            Console.WriteLine("R. El consumo del socio: {0}, correspondiente a: {1} {2} es de {3}. La deuda total es: {4}",member.ID, member.FirstName, member.SecondName, totalConsumption, total);
            Console.WriteLine("======================================");
        }
    }
}
